#!/usr/bin/env python
# -*- coding: utf-8 -*-

import nltk
from nltk.corpus import webtext
from nltk.probability import FreqDist

nltk.download('webtext')
wt_words = webtext.words('test.txt')
data_analysts = nltk.FreqDist(wt_words)

filter_words = dict([(m,n) for m, n in data_analysis.items() if len(m)> 3])

for key in sorted(filter_words):
    print("%s: %s" % (key, filter_words[key]))

###data_analysis = nltk.FreqDist(filter_words)
#data_anlysis.plot(25, cumulative=False)
