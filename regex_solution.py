#!/usr/bin/env python

import re #regex
import string #string
import csv
import stop_words
import matplotlib.pyplot as plt
import pandas as pd
#from nltk.tokenize import word_tokenize
from wordcloud import WordCloud, STOPWORDS
from stop_words import get_stop_words

frequency = {}
common_words = []
document_text = open('CA_one-star.csv', 'r')
#stop_words = set(stopwords.words('english'))
stop_words = get_stop_words('en')

text_string = document_text.read().lower()
match_pattern = re.findall(r'\b[a-z]{3,15}\b', text_string)

for word in match_pattern:
    if word not in stop_words:
        count = frequency.get(word,0)
        frequency[word] = count + 1

frequency_list = frequency.keys()

#print(stop_words)

for words in frequency_list:
    #print(words, frequency[words])
    x = 1    

with open('CA_one_stopped.csv', 'w') as f:
    #word_tokens = word_tokenize(document_text)
    writer = csv.writer(f)
    for k, v in frequency.items():
        writer.writerow([k, v])


###Messy implementation on the side
df = pd.read_csv('CA_all.csv')
comment_words = ''

for val in df:
    val = str(val)
    tokens = val.split()

    for i in range(len(tokens)):
        tokens[i] = tokens[i].lower()

    comment_words += " ".join(tokens)+ " "
    
wordcloud = WordCloud(width = 800, height = 800,
                      background_color = 'white',
                      stopwords = stop_words,
                      min_font_size = 10).generate_from_frequencies(frequency)

plt.figure(figsize = (8,8), facecolor = None)
plt.imshow(wordcloud)
plt.axis("off")
plt.tight_layout(pad=0)

plt.show()

