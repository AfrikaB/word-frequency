#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
from collections import Counter
from collections import defaultdict
words = []
words_counter = []

csv_file = raw_input('CA_five-star.csv')
txt_file = raw_input('CA_five-star.txt')
with open(txt_file, "w") as my_output_file:
    with open(csv_file, "r") as my_input_file:
        [my_output_file.write(" ".join(row)+'\n') for row in csv.reader(my_input_file)]
    my_output_file.close()

with open('CA_five-star.csv', 'rt') as csvfile:
    reader = csv.reader(csvfile)
    next(reader)
    for col in reader:
        csv_words = col[0].split(" ")
        for i in csv_words:
            words.append(i)

with open('CA_five_star_results.csv', 'a+') as csvfile:
    writer = csv.writer(csvfile, delimiter=',')
    for i in words:
        x = words.count(i)
        words_counter.append((i,x))
    writer.writerow(words_counter)
